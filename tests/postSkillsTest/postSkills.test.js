test('postSkills', async() => {
    const expectedTitle = 'testTitle'
    const response = await fetch("http://localhost:3000/skills", {
        method: "POST", 
        body: JSON.stringify({title: expectedTitle}),
        headers: {
            "Content-Type": "application/json",
          }
    });
    const newSkill = await response.json();
    const getSkillById = await (await fetch('http://localhost:3000/skills/' + newSkill.id)).json()
    expect(getSkillById.id).toBe(newSkill.id);
    expect(getSkillById.title).toBe(expectedTitle);
});