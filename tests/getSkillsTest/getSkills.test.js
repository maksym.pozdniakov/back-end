test('getSkills', async() => {
        const response = await fetch("http://localhost:3000/skills", {method: "GET"});
        const skillsList = await response.json();
  
          skillsList.forEach(skill => {
            expect(skill).toEqual(
            expect.objectContaining({   
              id: expect.any(Number),
              title: expect.any(String)              
            }))
          });
});