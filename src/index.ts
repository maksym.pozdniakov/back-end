import express from 'express'

const app = express()
const port = 3000

const jsonBodyMiddleware = express.json()
app.use(jsonBodyMiddleware)

const db = {
  skills: [
    { id: 1, title: 'front-end' },
    { id: 2, title: 'back-end' },
    { id: 3, title: 'automation qa' },
    { id: 4, title: 'devops' },
    { id: 5, title: 'dba' }
  ]
}

app.get('/skills', (req, res) => {
  let foundSkills = db.skills;

  if (req.query.title) {
    foundSkills = foundSkills
      .filter(s => s.title.indexOf(req.query.title as string) > -1)
  }

  res.json(foundSkills)
})

//@ts-ignore
const getSkillsById = (req, res) => {
  const foundSkill = db.skills.find(s => s.id === +req.params.id)

  if (!foundSkill) {
    res.sendStatus(404);
    return;
  }

  res.json(foundSkill)
}

app.get('/skills/:id', (req, res) => getSkillsById(req, res))
app.post('/skills', (req, res) => {
  const newSkill = {
    id: +(new Date()),
    title: req.body.title
  }
  console.log(req.body)
  db.skills.push(newSkill)
  console.log(newSkill)
  console.log(db.skills)
  res.json(newSkill)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
